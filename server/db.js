import mysql from 'mysql';

// Create a connection pool
export const pool = mysql.createPool({
  host: 'localhost',
  user: 'dragon',
  password: 'dragon',
  database: 'tcd_db',
});