import express from 'express';
import {getSterren, postSterren} from '../controllers/sterren.js';

const router = express.Router();

router.route('/')
  .post(postSterren)
  router.get('/:levelid', getSterren); 
export default router;