import { pool } from '../db.js';

//get sterren uit database en stuur aantal sterren spel krijgt
export const getSterren = (req, res) => {
  const levelid = req.params.levelid;
  const time = req.query.time;
    pool.getConnection((err, connection) => {
        if (err) {
          console.error('Error getting database connection:', err);
          return res.status(500).json({
            status: 'error',
            message: 'Internal Server Error'
          });
        }
      const query ="SELECT * FROM levels WHERE levelid = ? ORDER BY levels. time desc";
      const values = [levelid];
    connection.query(query,values,(error, results) => {
      connection.release();
        if (error) {
          console.error('Error fetching data: ' + error.stack);
          res.status(500).send('Error fetching data');
          return;
        }
       console.log("length" +results.length)
        const stars = calculateStars(time, results.length, results);
  
        res.json({ stars });
      }
    );
  });  
}
//spel in database steken
export const postSterren = (req, res) => {
  const { lvlid, tm } = req.body;
  console.log(lvlid)
  console.log(tm)
  const query ='INSERT INTO levels (id,levelid, time) VALUES (null,?, ?)';
  const values = [lvlid, tm];
  pool.getConnection((err, connection) => {
    if (err) {
      console.error('Error getting database connection:', err);
      return res.status(500).json({
        status: 'error',
        message: 'Internal Server Error'
      });
    }
    connection.query(query,values,(error, results) => {
      connection.release();
      if (error) {
        console.error('Error creating user:', error);
        return res.status(500).json({
          status: 'error',
          message: 'Internal Server Error'
        });
      }
        res
            .status(201)
            .json({ message: 'Level and time added successfully' });
      }
    );
  });
};
//bereken sterren functie
function calculateStars(time, totalPlayers, results) {
  // spelers in 5 groepen  delen
  const groups = 5;
  const playersPerGroup = totalPlayers / groups;
  if(playersPerGroup < 1){
    return 5;
  }

  // verkrijgen van positie in de lijst
  let position = 1;
  for (let i = 0; i < totalPlayers; i++) {
    if (time <= results[i].time) {
      console.log("for result time " + results[i].time)
      position = i;
    }
  }
  // bereken sterren adhv positie
  const stars = Math.floor(position / playersPerGroup);
  console.log("ppg" +playersPerGroup)
  console.log("position" + position)
  console.log("stars" + stars)


  return stars;
}
  