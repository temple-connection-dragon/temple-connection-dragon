import express from 'express';
import cors from 'cors';
import sterrenRouter from './routers/sterren.js';

const app = express();
app.use(cors({
  origin: true,
  methods: ["GET", "POST"],
  allowedHeaders: ["Origin", "X-API-Key", "X-Requested-With", "Content-Type", "Authorization"]
}));

app.use(express.json());

app.use('/getStars', sterrenRouter);

export { app };