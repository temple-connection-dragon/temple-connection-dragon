export let seconds = 0;
export let minutes = 0;
const appendMinutes = document.getElementById("minutes");
const appendSeconds = document.getElementById("seconds");
const buttonPause = document.querySelector('.pause');
const popup = document.querySelector(".popup")
const close = document.querySelector(".x")
let Interval ;
Interval = setInterval(startTimer, 1000);

export const stopT = function (){
    clearInterval(Interval);
}
buttonPause.addEventListener('click', function() {
    stopT();
    popup.classList.add("open")
})
close.addEventListener('click', function () {
    popup.classList.remove("open")
    Interval = setInterval(startTimer, 1000);
})
function startTimer () {
    seconds++;

    if(seconds <= 9){
        appendSeconds.innerHTML = "0" + seconds;
    }

    if (seconds > 9){
        appendSeconds.innerHTML = seconds;
    }

    if (seconds > 59) {
        console.log("minutes");
        minutes++;
        appendMinutes.innerHTML = "0" + minutes;
        seconds = 0;
        appendSeconds.innerHTML = "0" + 0;
    }
    if (minutes > 9){
        appendMinutes.innerHTML = minutes;
    }
    if(minutes>60){
        console.log("DNF");
        clearInterval(Interval);
    }
}