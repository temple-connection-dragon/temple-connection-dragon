const mutebtn = document.querySelector('.mute');
const muteIcon = document.querySelector('.muted_icon')
const audio = document.querySelector('audio')
let paused = localStorage.getItem('paused');

if (paused === 'off') {
    audio.pause();
    if (muteIcon) {
        muteIcon.src = "../assets/images/soundOff.png";
    }

} else {
    audio.play();
    if (muteIcon) {
        muteIcon.src = "../assets/images/sound.png";
    }
}
if (!mutebtn) {
    stop();
}else {
    mutebtn.addEventListener('click', function () {
        if (paused === 'off') {
            audio.play();
            localStorage.setItem('paused', 'on');
            paused = 'on';
            muteIcon.src = "../assets/images/sound.png";
        } else {
            audio.pause();
            localStorage.setItem('paused', 'off');
            paused = 'off';
            muteIcon.src = "../assets/images/soundOff.png";
        }
    })
}
