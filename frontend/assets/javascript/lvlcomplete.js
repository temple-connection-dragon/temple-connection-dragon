const urlParams = new URLSearchParams(window.location.search);
const time = urlParams.get('tijd');
const levelid = urlParams.get('id');
console.log("tijd " +time);
console.log("id " +levelid);
const showTime = document.getElementById("show_time");
const minutes = Math.floor(time / 60);
const seconds = time - minutes * 60;
showTime.textContent = minutes+":" + seconds + " minutes";
// Function to show star section based on the number of stars
function showStars(stars) {console.log("switch" + stars)
    switch (stars) {
        
        case 0:
            console.log("0sterren")
            document.querySelector('.zero').style.display = 'flex';
            break;
        case 1:
            console.log("1sterren")
            document.querySelector('.one').style.display = 'flex';
            break;
        case 2:
            console.log("2sterren")
            document.querySelector('.two').style.display = 'flex';
            break;
        case 3:
            console.log("3sterren")
            document.querySelector('.three').style.display = 'flex';
            break;
        case 4:
            console.log("4sterren")
            document.querySelector('.four').style.display = 'flex';
            break;
        default:
            break;
    }
}

//stuur net gespeelde tijd naar server 
const data = {
    lvlid: levelid,
    tm: time
  };

fetch(`http://localhost:3000/getStars`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then((response) => {
        if (response.ok) {
            console.log('tijd verstuurd naar server');
        } else {
            console.log('error bij het versturen van tijd naar server');
        }
    })

// verkijg aantal sterren adhv net gespeelde tijd
async function fetchstars() {
    try {
        console.log("fetching stars")
        const response = await fetch(`http://localhost:3000/getStars/${levelid}?time=${time}`);
        if (!response.ok) {
            throw new Error('Request failed');
        }
        const stars = await response.json();
        showStars(stars.stars);
        console.log( stars.stars);
    } catch (error) {
        console.log('Error:', error);
        // Handle the error gracefully (e.g., show an error message to the user)
    }
}

fetchstars();