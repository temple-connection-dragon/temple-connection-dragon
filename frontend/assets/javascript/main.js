console.log(localStorage.getItem("selectedlvl"))
import {stopT, seconds, minutes} from "./timer.js";

class GridSystem {
    constructor(matrix, poortMatrix, finalPieceAlign) {
        this.matrix = matrix;
        this.poortmatrix = poortMatrix;
        this.finalPieceAlign = finalPieceAlign;
        this.uiContext = this.#getContext(420, 580, "#000");
        this.outlineContext = this.#getContext(0, 0, "#444");
        this.topContext = this.#getContext(0, 0, "#444", true);
        this.excludeBtn = []
        this.cellSize = 50;
        this.padding = 0.5;
        this.rotations = 0
        this.topmatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        this.piece = {
            currentPiece: [
                [0, 0], [0, 0]
            ],
            pieceX: 0,
            pieceY: 0
        }
        document.getElementById("1").addEventListener('click', () => {
            document.removeEventListener("keydown", this.#movePlayer);
            document.addEventListener("keydown", this.#movePlayer);
            this.#tempLeeg()
            this.#renderTemp()
            this.#renderTop();
            this.rotations = 0;
            this.piece = {
                id: 1,
                r1: [
                    [92, 91],
                    [812, 0]
                ],
                r2: [
                    [811, 91],
                    [0, 93]
                ],
                r3: [
                    [0, 812],
                    [94, 93]
                ],
                r4: [
                    [92, 0],
                    [94, 811]
                ],
                currentPiece: [
                    [92, 91],
                    [812, 0]
                ],
                pieceX: 1,
                pieceY: 3
            }
            if (!this.excludeBtn.find(value => value === 1)) {
                this.#drawPice();
            } else {
                this.piece = {
                    currentPiece: [
                        [0, 0], [0, 0]
                    ],
                    pieceX: 0,
                    pieceY: 0
                }
            }
        })

        document.getElementById("2").addEventListener('click', () => {
            document.removeEventListener("keydown", this.#movePlayer);
            document.addEventListener("keydown", this.#movePlayer);
            this.#tempLeeg()
            this.#renderTemp()
            this.#renderTop();
            this.rotations = 0;
            this.piece = {
                id: 2,
                r1: [
                    [92, 91],
                    [0, 812]
                ],
                r2: [
                    [0, 91],
                    [811, 93]
                ],
                r3: [
                    [812, 0],
                    [94, 93]
                ],
                r4: [
                    [92, 811],
                    [94, 0]
                ],
                currentPiece:
                    [
                        [92, 91],
                        [0, 812]
                    ],
                pieceX: 1,
                pieceY: 3
            }
            if (!this.excludeBtn.find(value => value === 2)) {
                this.#drawPice();
            } else {
                this.piece = {
                    currentPiece: [
                        [0, 0], [0, 0]
                    ],
                    pieceX: 0,
                    pieceY: 0
                }
            }
        })

        document.getElementById("3").addEventListener('click', () => {
            document.removeEventListener("keydown", this.#movePlayer);
            document.addEventListener("keydown", this.#movePlayer);
            this.#tempLeeg()
            this.#renderTemp()
            this.#renderTop();
            this.rotations = 0;
            this.piece = {
                id: 3,
                r1: [
                    [92, 821],
                    [812, 0]
                ],
                r2: [
                    [811, 91],
                    [0, 823]
                ],
                r3: [
                    [0, 812],
                    [822, 93]
                ],
                r4: [
                    [824, 0],
                    [94, 811]
                ],
                currentPiece:
                    [
                        [92, 821],
                        [812, 0]
                    ],
                pieceX: 1,
                pieceY: 3
            }
            if (!this.excludeBtn.find(value => value === 3)) {
                this.#drawPice();
            } else {
                this.piece = {
                    currentPiece: [
                        [0, 0], [0, 0]
                    ],
                    pieceX: 0,
                    pieceY: 0
                }
            }
        })

        document.getElementById("4").addEventListener('click', () => {
            document.removeEventListener("keydown", this.#movePlayer);
            document.addEventListener("keydown", this.#movePlayer);
            this.#tempLeeg()
            this.#renderTemp()
            this.#renderTop();
            this.rotations = 0;
            this.piece = {
                id: 4,
                r1: [
                    [822, 91],
                    [0, 812]
                ],
                r2: [
                    [0, 824],
                    [811, 93]
                ],
                r3: [
                    [812, 0],
                    [94, 821]
                ],
                r4: [
                    [92, 811],
                    [823, 0]
                ],
                currentPiece:
                    [
                        [822, 91],
                        [0, 812]
                    ],
                pieceX: 1,
                pieceY: 3
            }
            if (!this.excludeBtn.find(value => value === 4)) {
                this.#drawPice();
            } else {
                this.piece = {
                    currentPiece: [
                        [0, 0], [0, 0]
                    ],
                    pieceX: 0,
                    pieceY: 0
                }
            }
        })

        document.getElementById("5").addEventListener('click', () => {
            document.removeEventListener("keydown", this.#movePlayer);
            document.addEventListener("keydown", this.#movePlayer);
            this.#tempLeeg()
            this.#renderTemp()
            this.#renderTop();
            this.rotations = 0;
            this.piece = {
                id: 5,
                r1: [
                    [92, 0],
                    [812, 0]
                ],
                r2: [
                    [811, 91],
                    [0, 0]
                ],
                r3: [
                    [0, 812],
                    [0, 93]
                ],
                r4: [
                    [0, 0],
                    [94, 811]
                ],
                currentPiece:
                    [
                        [92, 0],
                        [812, 0]
                    ],
                pieceX: 1,
                pieceY: 3
            }
            if (!this.excludeBtn.find(value => value === 5)) {
                this.#drawPice();
            } else {
                this.piece = {
                    currentPiece: [
                        [0, 0], [0, 0]
                    ],
                    pieceX: 0,
                    pieceY: 0
                }
            }
        })

        document.getElementById("6").addEventListener('click', () => {
            document.addEventListener("keydown", this.#movePlayer);
            this.#tempLeeg()
            this.#renderTemp()
            this.#renderTop();
            this.rotations = 0;
            this.piece = {
                id: 6,
                r1: [
                    [92, 821],
                    [94, 0]
                ],
                r2: [
                    [92, 91],
                    [0, 823]
                ],
                r3: [
                    [0, 91],
                    [822, 93]
                ],
                r4: [
                    [824, 0],
                    [94, 93]
                ],
                currentPiece:
                    [
                        [92, 821],
                        [94, 0]
                    ],
                pieceX: 1,
                pieceY: 3
            }
            if (!this.excludeBtn.find(value => value === 6)) {
                this.#drawPice();
            } else {
                this.piece = {
                    currentPiece: [
                        [0, 0], [0, 0]
                    ],
                    pieceX: 0,
                    pieceY: 0
                }
            }
        })

        document.getElementById("7").addEventListener('click', () => {
            document.removeEventListener("keydown", this.#movePlayer);
            document.addEventListener("keydown", this.#movePlayer);
            this.#renderTop();
            this.#tempLeeg()
            this.rotations = 0;
            this.piece = {
                id: 7,
                r1: [
                    [92, 821],
                    [93, 0]
                ],
                r2: [
                    [94, 91],
                    [0, 823]
                ],
                r3: [
                    [0, 92],
                    [822, 93]
                ],
                r4: [
                    [824, 0],
                    [94, 91]
                ],
                currentPiece:
                    [
                        [92, 821],
                        [93, 0]
                    ],
                pieceX: 1,
                pieceY: 3
            }
            if (!this.excludeBtn.find(value => value === 7)) {
                this.#drawPice();
            } else {
                this.piece = {
                    currentPiece: [
                        [0, 0], [0, 0]
                    ],
                    pieceX: 0,
                    pieceY: 0
                }
            }
        })

        document.getElementById("set").addEventListener('click', () => {
            document.removeEventListener("keydown", this.#movePlayer);
            let valid = true
            for (let row = 0; row < this.piece.currentPiece.length; row++) {
                for (let col = 0; col < this.piece.currentPiece[row].length; col++) {
                    const cellvalTop = this.topmatrix[row + this.piece.pieceX][col + this.piece.pieceY]
                    const cellvalPiece = this.piece.currentPiece[row][col]
                    const cellvalBottom = this.matrix[row + this.piece.pieceX][col + this.piece.pieceY]
                    const cellvalPoort = this.poortmatrix[row + this.piece.pieceX][col + this.piece.pieceY]
                    valid = (this.#checkValidPiece(cellvalPiece, cellvalTop, cellvalBottom, cellvalPoort) && valid)
                    console.log(valid)
                }
            }
            if (valid) {
                this.excludeBtn.push(this.piece.id);
                for (let row = 0; row < this.piece.currentPiece.length; row++) {
                    for (let col = 0; col < this.piece.currentPiece[row].length; col++) {
                        if (this.piece.currentPiece[row][col] !== 0) {
                            if (this.topmatrix[row + this.piece.pieceX][col + this.piece.pieceY] === 0)
                                this.topmatrix[row + this.piece.pieceX][col + this.piece.pieceY] = this.piece.currentPiece[row][col];
                            else if (this.piece.currentPiece[row][col] > 820) {
                                if (this.topmatrix[row + this.piece.pieceX][col + this.piece.pieceY] > this.piece.currentPiece[row][col])
                                    this.topmatrix[row + this.piece.pieceX][col + this.piece.pieceY] = [this.piece.currentPiece[row][col], this.topmatrix[row + this.piece.pieceX][col + this.piece.pieceY]]
                                else
                                    this.topmatrix[row + this.piece.pieceX][col + this.piece.pieceY] = [this.topmatrix[row + this.piece.pieceX][col + this.piece.pieceY], this.piece.currentPiece[row][col]]

                            } else {
                                this.topmatrix[row + this.piece.pieceX][col + this.piece.pieceY] = [this.piece.currentPiece[row][col], this.topmatrix[row + this.piece.pieceX][col + this.piece.pieceY]]
                            }
                        }
                    }
                }
            }
            console.log(this.topmatrix)
            this.piece = {
                currentPiece: [
                    [0, 0], [0, 0]
                ],
                pieceX: 0,
                pieceY: 0
            }
            this.#renderTop()
        })

        document.getElementById("rotate").addEventListener('click', () => {
            this.rotations++;
            if (this.rotations === 1)
                this.piece.currentPiece = this.piece.r2;
            else if (this.rotations === 2)
                this.piece.currentPiece = this.piece.r3;
            else if (this.rotations === 3)
                this.piece.currentPiece = this.piece.r4;
            else if (this.rotations === 4) {
                this.piece.currentPiece = this.piece.r1;
                this.rotations = 0;
            }
            this.#updateMatrix(this.piece.pieceX, this.piece.pieceY)
            this.#renderTemp();

        })

        document.getElementById("check").addEventListener('click', () => {
            console.log(this.finalPieceAlign)
            console.log(this.topmatrix)
            if (this.#checkArrays(this.finalPieceAlign, this.topmatrix)) {
                console.log("succes");
                stopT()
                let time = minutes * 60 + seconds
                window.location.assign(`../succes/index.html?id=${localStorage.getItem("selectedlvl")}&tijd=${time}`)
            } else {
                this.#reset();
            }
        })

        document.getElementById("reset").addEventListener('click', () => this.#reset())

    }

    #checkValidPiece(pOp, place, placeB, poort) {
        if (pOp === 0) {
            return true;
        }
        if (!place.length) {
            if (pOp === place || (place === placeB && placeB === 0)) {
                return true
            } else if (place === 31 || place === 32 || place === 33) {
                return false
            } else if (placeB === 22 || placeB === 23 || placeB === 21) {
                if (placeB === 21 && (pOp === 821 || pOp === 822 || pOp === 823 || pOp === 824) && place === 0) {
                    return true
                } else if (placeB === 21 && (pOp === 821 || pOp === 822 || pOp === 823 || pOp === 824) && (place === 821 || place === 822 || place === 823 || place === 824)) {
                    return true
                }
                return false
            } else if (place === 811 || place === 812) {
                if (pOp === 821 || pOp === 822 || pOp === 823 || pOp === 824) {
                    return true
                }
            } else if (poort > 320) {
                if ((pOp === 91 || pOp === 92 || pOp === 93 || pOp === 94))
                    return true
                else if (pOp === 811 || pOp === 812) {
                    return true
                } else if (poort === 321 && pOp === 822) {
                    return true
                } else if (poort === 322 && pOp === 821) {
                    return true
                } else if (poort === 323 && pOp === 824) {
                    return true
                } else if (poort === 324 && pOp === 823) {
                    return true
                }
            } else if (place === 821 || place === 822 || place === 823 || place === 824) {
                if ((pOp === 91 || pOp === 92 || pOp === 93 || pOp === 94))
                    return true
                else if (pOp === 811 || pOp === 812)
                    return true
                else if (pOp === 821 || pOp === 822 || pOp === 823 || pOp === 824)
                    return true

            } else if (poort === 321 || poort === 311 || poort === 331)
                if (pOp === 821 || pOp === 822 || pOp === 823 || pOp === 824)
                    return true
        } else {
            place.forEach(p => {
            })
        }
        return false;
    }

    #tempLeeg() {
        this.tempM = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        for (let i = 0; i < this.topmatrix.length; i++) {
            for (let j = 0; j < this.topmatrix[i].length; j++) {
                this.tempM[i][j] = this.topmatrix[i][j];
            }
        }
    }

    #isValidMove(x, y) {
        if (this.tempM[this.piece.pieceY + y][this.piece.pieceX + x] !== 1) {
            return true;
        }
        return false;
    }

    #updateMatrix(x, y) {
        this.#tempLeeg()
        if (this.piece.currentPiece[0][0] !== 0) {
            this.tempM[x][y] = this.piece.currentPiece[0][0]
        }
        if (this.piece.currentPiece[0][1] !== 0) {
            this.tempM[x][y + 1] = this.piece.currentPiece[0][1]
        }
        if (this.piece.currentPiece[1][0] !== 0) {
            this.tempM[x + 1][y] = this.piece.currentPiece[1][0]
        }
        if (this.piece.currentPiece[1][1] !== 0) {
            this.tempM[x + 1][y + 1] = this.piece.currentPiece[1][1]
        }
    }

    #movePlayer = ({keyCode}) => {
        if (keyCode === 38) {
            if (this.#isValidMove(-1, 0)) {
                this.piece.pieceX--;
            }
        } else if (keyCode === 40) {
            if (this.#isValidMove(2, 0)) {
                this.piece.pieceX++;

            }
        } else if (keyCode === 37) {
            if (this.#isValidMove(0, -1)) {
                this.piece.pieceY--;

            }
        } else if (keyCode === 39) {
            if (this.#isValidMove(0, 2)) {
                this.piece.pieceY++;

            }
        }
        this.#updateMatrix(this.piece.pieceX, this.piece.pieceY)
        this.#renderTemp();
    }

    #getCenter(w, h) {
        return {
            x: window.innerWidth / 2 - w / 2 + "px",
            y: window.innerHeight / 2 - h / 2 + "px"
        };
    }

    #getContext(w, h, color = "#111", isTransparent = false) {
        const canvasSection = document.querySelector(".canvas_section")
        this.canvas = document.createElement("canvas");
        this.context = this.canvas.getContext("2d");
        this.canvas.width = w;
        this.canvas.height = h;
        this.canvas.style.position = "absolute";
        this.canvas.style.background = color;
        if (isTransparent) {
            this.canvas.style.backgroundColor = "transparent";
        }
        const center = this.#getCenter(w, h);
        this.canvas.style.marginLeft = center.x
        this.canvas.style.marginTop = center.y;
        canvasSection.appendChild(this.canvas);
        return this.context;
    }

    #checkTempel(value) {
        if (value >= 330) {
            return 33;
        } else if (value >= 320) {
            return 32;
        } else if (value >= 310) {
            return 31;
        }
    }

    #checkRotation(portx, porty, tempelx, tempely) {
        if (portx === tempelx && porty === tempely + 1) {
            return "h-up"
        }
        if (portx === tempelx && porty === tempely - 1) {
            return "h-down"
        }
        if (portx === tempelx + 1 && porty === tempely) {
            return "v-left"
        }
        if (portx === tempelx - 1 && porty === tempely) {
            return "v-right"
        }
    }

    #checkLocTempel(tempel, portx, porty) {
        for (let row = 0; row < this.matrix.length; row++) {
            for (let col = 0; col < this.matrix[row].length; col++) {
                if (this.matrix[row][col] === tempel) {
                    return this.#checkRotation(portx, porty, col, row)
                }
            }
        }
    }

    #drawPice() {
        for (let row = 0; row < this.piece.currentPiece.length; row++) {
            for (let col = 0; col < this.piece.currentPiece[row].length; col++) {
                const cellVal = this.piece.currentPiece[row][col];
                this.#checkPiece(cellVal, row, col, this.outlineContext)
            }
        }
    }

    #drawPieceOfPiece(color, rotation, row, col, context, bocht) {
        row = row * (this.cellSize + this.padding)
        col = col * (this.cellSize + this.padding)
        context.fillStyle = color
        if (bocht) {
            if (rotation === "rechts onder") {
                context.moveTo(row, col)
                context.beginPath();
                context.arc(row, col, this.cellSize * 0.75, 0, 1.57);
                context.lineTo(row, col)
                context.lineTo(row + this.cellSize, col)
                context.fill()
                context.closePath()
                context.fillStyle = "#444"
                context.beginPath();
                context.arc(row, col, this.cellSize / 4, 0, 1.57);
                context.lineTo(row, col)
                context.lineTo(row + this.cellSize / 2, col)

            }
            if (rotation === "links onder") {
                row += this.cellSize
                context.moveTo(row, col)
                context.beginPath();
                context.arc(row, col, this.cellSize * 0.75, 1.57, 1.57 * 2);
                context.lineTo(row, col)
                context.lineTo(row, col + this.cellSize)
                context.fill()
                context.closePath()
                context.fillStyle = "#444"
                context.beginPath();
                context.arc(row, col, this.cellSize / 4, 1.57, 1.57 * 2);
                context.lineTo(row, col)
                context.lineTo(row, col + this.cellSize / 2)

            }
            if (rotation === "links boven") {
                row += this.cellSize
                col += this.cellSize
                context.moveTo(row, col)
                context.beginPath();
                context.arc(row, col, this.cellSize * 0.75, 1.57 * 2, 1.57 * 3);
                context.lineTo(row, col)
                context.lineTo(row, col)
                context.fill()
                context.closePath()
                context.fillStyle = "#444"
                context.beginPath();
                context.arc(row, col, this.cellSize / 4, 1.57 * 2, 1.57 * 3);
                context.lineTo(row, col)
                context.lineTo(row, col)

            }
            if (rotation === "rechts boven") {
                col += this.cellSize;
                context.moveTo(row, col)
                context.beginPath();
                context.arc(row, col, this.cellSize * 0.75, 1.57 * 3, 1.57 * 4);
                context.lineTo(row, col)
                context.lineTo(row, col - this.cellSize / 2)
                context.fill()
                context.closePath()
                context.fillStyle = "#444"
                context.beginPath();
                context.arc(row, col, this.cellSize / 4, 1.57 * 3, 1.57 * 4);
                context.lineTo(row, col)
                context.lineTo(row, col - this.cellSize / 4)
            }
            context.fill()
            context.closePath()
        } else if (rotation.indexOf("hoog") !== -1) {
            if (rotation === 'hoog-o') {
                context.fillRect(col + this.cellSize / 4,
                    row + this.cellSize / 2,
                    this.cellSize / 2, this.cellSize / 2);

            } else if (rotation === 'hoog-l') {
                context.fillRect(col,
                    row + this.cellSize / 4,
                    this.cellSize / 2, this.cellSize / 2);
            } else if (rotation === 'hoog-r') {
                context.fillRect(col + this.cellSize / 2,
                    row + this.cellSize / 4,
                    this.cellSize / 2, this.cellSize / 2);

            } else if (rotation === 'hoog-b') {
                context.fillRect(col + this.cellSize / 4,
                    row,
                    this.cellSize / 2, this.cellSize / 2);
            }
        } else {
            if (rotation === "recht-h") {
                context.fillRect(col, row + this.cellSize / 4,
                    this.cellSize, this.cellSize / 2);
            } else {
                context.fillRect(col + this.cellSize / 4, row,
                    this.cellSize / 2, this.cellSize);
            }
        }
    }

    #drawPort(color, rotation, row, col, traingle) {
        this.outlineContext.fillStyle = color;
        if (!traingle) {
            if (rotation === "v-left") {
                this.outlineContext.fillRect(col * (this.cellSize + this.padding),
                    row * (this.cellSize + this.padding) + this.cellSize / 4,
                    this.cellSize / 2, this.cellSize / 2);
            }
            if (rotation === "v-right") {
                this.outlineContext.fillRect(col * (this.cellSize + this.padding) + this.cellSize / 2,
                    row * (this.cellSize + this.padding) + this.cellSize / 4,
                    this.cellSize / 2, this.cellSize / 2);
            }
            if (rotation === "h-up") {
                this.outlineContext.fillRect(col * (this.cellSize + this.padding) + this.cellSize / 4,
                    row * (this.cellSize + this.padding),
                    this.cellSize / 2, this.cellSize / 2);
            }
            if (rotation === "h-down") {
                this.outlineContext.fillRect(col * (this.cellSize + this.padding) + this.cellSize / 4,
                    row * (this.cellSize + this.padding) + this.cellSize / 2,
                    this.cellSize / 2, this.cellSize / 2);
            }
        } else {
            this.outlineContext.beginPath()
            if (rotation === "v-left") {
                this.outlineContext.moveTo((col * (this.cellSize + this.padding) + this.cellSize / 2) - this.cellSize / 2, (row * (this.cellSize + this.padding) + this.cellSize / 2))
                this.outlineContext.lineTo((col * (this.cellSize + this.padding)) - this.cellSize / 4, (row * (this.cellSize + this.padding) + this.cellSize - this.cellSize / 4))
                this.outlineContext.lineTo((col * (this.cellSize + this.padding)) - this.cellSize / 4, (row * (this.cellSize + this.padding) + this.cellSize / 4))
            }
            if (rotation === "v-right") {
                this.outlineContext.moveTo(col * (this.cellSize + this.padding) + this.cellSize + this.cellSize / 4, row * (this.cellSize + this.padding) + this.cellSize / 4)
                this.outlineContext.lineTo(col * (this.cellSize + this.padding) + this.cellSize + this.cellSize / 4, row * (this.cellSize + this.padding) + this.cellSize * 0.75)
                this.outlineContext.lineTo(col * (this.cellSize + this.padding) + this.cellSize, row * (this.cellSize + this.padding) + this.cellSize / 2)
            }
            if (rotation === "h-up") {
                this.outlineContext.moveTo(col * (this.cellSize + this.padding) + this.cellSize / 4, row * (this.cellSize + this.padding) - this.cellSize / 4)
                this.outlineContext.lineTo(col * (this.cellSize + this.padding) + this.cellSize * 0.75, row * (this.cellSize + this.padding) - this.cellSize / 4)
                this.outlineContext.lineTo(col * (this.cellSize + this.padding) + this.cellSize / 2, row * (this.cellSize + this.padding))
            }
            if (rotation === "h-down") {
                this.outlineContext.moveTo(col * (this.cellSize + this.padding) + this.cellSize * 0.75, row * (this.cellSize + this.padding) + this.cellSize + this.cellSize / 4)
                this.outlineContext.lineTo(col * (this.cellSize + this.padding) + this.cellSize / 2, row * (this.cellSize + this.padding) + this.cellSize)
                this.outlineContext.lineTo(col * (this.cellSize + this.padding) + this.cellSize / 4, row * (this.cellSize + this.padding) + this.cellSize + this.cellSize / 4)
            }
            this.outlineContext.fill()
            this.outlineContext.closePath()
        }
    }

    #checkPiece(cellVal, row, col, context) {
        if (cellVal === 811) {
            this.#drawPieceOfPiece("#b44610", "recht-h", this.piece.pieceX + row, this.piece.pieceY + col, context);
        } else if (cellVal === 812) {
            this.#drawPieceOfPiece("#b44610", "recht-v", this.piece.pieceX + row, this.piece.pieceY + col, context);
        } else if (cellVal === 821) {
            this.#drawPieceOfPiece("#ff6f29", "hoog-l", this.piece.pieceX + row, this.piece.pieceY + col, context);
        } else if (cellVal === 822) {
            this.#drawPieceOfPiece("#ff6f29", "hoog-r", this.piece.pieceX + row, this.piece.pieceY + col, context);
        } else if (cellVal === 823) {
            this.#drawPieceOfPiece("#ff6f29", "hoog-b", this.piece.pieceX + row, this.piece.pieceY + col, context);
        } else if (cellVal === 824) {
            this.#drawPieceOfPiece("#ff6f29", "hoog-o", this.piece.pieceX + row, this.piece.pieceY + col, context);
        } else if (cellVal === 91) {
            this.#drawPieceOfPiece("#b44610", "rechts boven", this.piece.pieceY + col, this.piece.pieceX + row, context, true)
        } else if (cellVal === 92) {
            this.#drawPieceOfPiece("#b44610", "links boven", this.piece.pieceY + col, this.piece.pieceX + row, context, true)
        } else if (cellVal === 93) {
            this.#drawPieceOfPiece("#b44610", "rechts onder", this.piece.pieceY + col, this.piece.pieceX + row, context, true)
        } else if (cellVal === 94) {
            this.#drawPieceOfPiece("#b44610", "links onder", this.piece.pieceY + col, this.piece.pieceX + row, context, true)
        }
    }

    #renderTemp() {
        this.topContext.clearRect(0, 0, this.canvas.width, this.canvas.height)
        this.render();
        for (let row = 0; row < this.tempM.length; row++) {
            for (let col = 0; col < this.tempM[row].length; col++) {
                const cellVal = this.tempM[row][col];
                if (cellVal === 0) {
                    this.topContext.fillStyle = "transparent";
                    this.topContext.fillRect(col * (this.cellSize + this.padding),
                        row * (this.cellSize + this.padding),
                        this.cellSize, this.cellSize);
                } else {
                    this.#checkPiece(cellVal, row - this.piece.pieceX, col - this.piece.pieceY, this.topContext)
                }
            }
        }
    }

    #renderTop() {
        this.outlineContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.render()
        for (let row = 0; row < this.topmatrix.length; row++) {
            for (let col = 0; col < this.topmatrix[row].length; col++) {
                const cellVal = this.topmatrix[row][col];
                if (cellVal === 0) {
                    this.outlineContext.fillStyle = "transparent";
                    this.outlineContext.fillRect(col * (this.cellSize + this.padding),
                        row * (this.cellSize + this.padding),
                        this.cellSize, this.cellSize);
                } else {
                    if (Array.isArray(cellVal)) {
                        cellVal.forEach(v => {
                            this.#checkPiece(v, row - this.piece.pieceX, col - this.piece.pieceY, this.outlineContext)
                        })
                    } else {
                        this.#checkPiece(cellVal, row - this.piece.pieceX, col - this.piece.pieceY, this.outlineContext)
                    }
                }
            }
        }
        this.renderPoort()
    }

    renderPoort() {
        for (let row = 0; row < this.poortmatrix.length; row++) {
            for (let col = 0; col < this.poortmatrix[row].length; col++) {
                const cellVal = this.poortmatrix[row][col];
                let color = "#111";
                if (cellVal === 311 || cellVal === 321 || cellVal === 331) {
                    color = "#000"
                    const rotation = this.#checkLocTempel(this.#checkTempel(cellVal), col, row)
                    this.#drawPort(color, rotation, row, col, true)
                } else if (cellVal === 322 || cellVal === 323 || cellVal === 324 || cellVal === 325) {
                    color = "#ff6f29"
                    let rotation = ""
                    if (cellVal === 322) {
                        rotation = "v-left"
                    } else if (cellVal === 323) {
                        rotation = "v-right"
                    } else if (cellVal === 324) {
                        rotation = "h-up"
                    } else if (cellVal === 325) {
                        rotation = "h-down"
                    }
                    this.#drawPort(color, rotation, row, col)

                } else if (Array.isArray(cellVal)) {
                    cellVal.forEach(v => {
                        const rotation = this.#checkLocTempel(this.#checkTempel(v), col, row)
                        if (v === 322 || v === 323 || v === 324 || v === 325) {
                            color = "#ff6f29"
                            let rotation = ""
                            if (v === 322) {
                                rotation = "v-left"
                            } else if (v === 323) {
                                rotation = "v-right"
                            } else if (v === 324) {
                                rotation = "h-up"
                            } else if (v === 325) {
                                rotation = "h-down"
                            }
                            this.#drawPort(color, rotation, row, col)
                        } else if (v === 311 || v === 321 || v === 331) {
                            color = "#000"
                            this.#drawPort(color, rotation, row, col, true)
                        }
                    })
                }
            }
        }
        this
            .uiContext
            .font = "20px Courier";
        this
            .uiContext
            .fillStyle = "white";
        this
            .uiContext
            .fillText(
                "Tempel connection dragon edition"
                ,
                20
                ,
                30
            )
        ;
    }

    render() {
        const w = (this.cellSize + this.padding) * this.matrix[0].length - (this.padding);
        const h = (this.cellSize + this.padding) * this.matrix.length - (this.padding);

        this.outlineContext.canvas.width = w;
        this.outlineContext.canvas.height = h;
        this.topContext.canvas.height = h;
        this.topContext.canvas.width = w;

        const center = this.#getCenter(w, h);
        this.outlineContext.canvas.style.marginLeft = center.x
        this.outlineContext.canvas.style.marginTop = center.y;

        this.topContext.canvas.style.marginLeft = center.x
        this.topContext.canvas.style.marginTop = center.y;

        for (let row = 0; row < this.matrix.length; row++) {
            for (let col = 0; col < this.matrix[row].length; col++) {
                const cellVal = this.matrix[row][col];
                let color = "#111";
                if (cellVal === 1) {
                    color = "#4488FF";
                    this.outlineContext.fillStyle = color;
                    this.outlineContext.fillRect(col * (this.cellSize + this.padding),
                        row * (this.cellSize + this.padding),
                        this.cellSize, this.cellSize);
                } else if (cellVal === 21 || cellVal === 22 || cellVal === 23) {
                    color = "#ffef44";
                    if (cellVal === 21)
                        color = "#afa01c"
                    this.outlineContext.fillStyle = color;
                    this.outlineContext.fillRect(col * (this.cellSize + this.padding),
                        row * (this.cellSize + this.padding),
                        this.cellSize, this.cellSize);

                } else if (cellVal === 31 || cellVal === 32 || cellVal === 33) {
                    color = "#ff6f29";
                    this.outlineContext.fillStyle = color;
                    this.outlineContext.fillRect(col * (this.cellSize + this.padding),
                        row * (this.cellSize + this.padding),
                        this.cellSize, this.cellSize);
                }
            }
        }
        for (let row = 0; row < this.poortmatrix.length; row++) {
            for (let col = 0; col < this.poortmatrix[row].length; col++) {
                const cellVal = this.poortmatrix[row][col];
                let color = "#111";
                if (cellVal === 311 || cellVal === 321 || cellVal === 331) {
                    color = "#000"
                    const rotation = this.#checkLocTempel(this.#checkTempel(cellVal), col, row)
                    this.#drawPort(color, rotation, row, col, true)
                } else if (cellVal === 322 || cellVal === 323 || cellVal === 324 || cellVal === 325) {
                    color = "#ff6f29"
                    let rotation = ""
                    if (cellVal === 322) {
                        rotation = "v-left"
                    } else if (cellVal === 323) {
                        rotation = "v-right"
                    } else if (cellVal === 324) {
                        rotation = "h-up"
                    } else if (cellVal === 325) {
                        rotation = "h-down"
                    }
                    this.#drawPort(color, rotation, row, col)

                } else if (Array.isArray(cellVal)) {
                    cellVal.forEach(v => {
                        const rotation = this.#checkLocTempel(this.#checkTempel(v), col, row)
                        if (v === 322 || v === 323 || v === 324 || v === 325) {
                            color = "#ff6f29"
                            let rotation = ""
                            if (v === 322) {
                                rotation = "v-left"
                            } else if (v === 323) {
                                rotation = "v-right"
                            } else if (v === 324) {
                                rotation = "h-up"
                            } else if (v === 325) {
                                rotation = "h-down"
                            }
                            this.#drawPort(color, rotation, row, col)
                        } else if (v === 311 || v === 321 || v === 331) {
                            color = "#000"
                            this.#drawPort(color, rotation, row, col, true)
                        }
                    })
                }
            }
        }
        this
            .uiContext
            .font = "20px Courier";
        this
            .uiContext
            .fillStyle = "white";
        this
            .uiContext
            .fillText(
                "Tempel connection dragon edition"
                ,
                20
                ,
                30
            )
        ;
    }

    #reset() {
        console.log("reset")
        this.excludeBtn = [];
        this.topmatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        this.#renderTop()
        this.#tempLeeg()
        this.#renderTemp()

    }

    #checkArrays(arr1, arr2) {
        if (arr1.length !== arr2.length) {
            return false;
        }

        // Iterate over each sub-array
        for (let i = 0; i < arr1.length; i++) {
            // Check if the sub-arrays have the same length
            if (arr1[i].length !== arr2[i].length) {
                return false;
            }

            // Iterate over each element in the sub-arrays
            for (let j = 0; j < arr1[i].length; j++) {
                // Compare the elements
                if (Array.isArray(arr1[i][j]) && Array.isArray(arr2[i][j])) {
                    if (arr1[i][j].length !== arr2[i][j].length) {
                        console.log("l")
                        return false;
                    }
                    for (let k = 0; k < arr1[i][j].length; k++) {
                        if (arr1[i][j][k] !== arr2[i][j][k]) {
                            console.log("s")
                            return false
                        }
                    }
                } else if (arr1[i][j] !== arr2[i][j]) {
                    return false;
                }
            }
        }

        // All elements are equal
        return true;
    }

}

let gridMatrix
let poortMatrix
let finalPieceAlign
console.log(localStorage.getItem("selectedlvl"))
switch (+localStorage.getItem("selectedlvl")) {
    //easy
    case 1:
        //lvl1
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 33, 0, 0, 0, 1],
            [1, 32, 0, 23, 21, 22, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 323, 33, 0, 0, 0, 1],
            [1, 32, [322, 331], 0, 0, 0, 1],
            [1, 321, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 812, 0, 0, 0, 1],
            [1, 94, 93, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break;
    case 2:
        //lvl2
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 22, 21, 23, 0, 0, 1],
            [1, 0, 0, 0, 31, 0, 1],
            [1, 0, 0, 0, 0, 32, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 311, 0, 1],
            [1, 0, 0, 0, 31, [311, 325], 1],
            [1, 0, 0, 0, 0, 32, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 92, 91, 1],
            [1, 0, 0, 0, 0, 823, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 3:
        // lvl3
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 33, 0, 0, 0, 1],
            [1, 0, 0, 23, 21, 22, 1],
            [1, 0, 0, 0, 32, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 33, 331, 0, 0, 1],
            [1, 0, 324, 0, 0, 0, 1],
            [1, 0, 0, 321, 32, 0, 1],
            [1, 0, 0, 0, 324, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 824, 0, 0, 0, 1],
            [1, 0, 94, 811, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 4:
        // lvl4
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 31, 0, 0, 0, 0, 1],
            [1, 0, 0, 33, 0, 23, 1],
            [1, 0, 0, 0, 0, 21, 1],
            [1, 0, 0, 0, 0, 22, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 31, 311, 331, 0, 0, 1],
            [1, 0, 0, 33, 322, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 811, 91, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 5:
        //lvl5
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 31, 0, 0, 1],
            [1, 0, 0, 23, 21, 22, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 33, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 311, 31, 0, 0, 1],
            [1, 0, 0, 311, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 323, 33, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 92, 0, 0, 0, 1],
            [1, 0, 812, 0, 0, 0, 1],
            [1, 0, 812, 0, 0, 0, 1],
            [1, 0, 94, 821, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 6:
//lvl6
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 23, 21, 22, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 31, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 32, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 311, 0, 1],
            [1, 0, 0, 0, 31, 311, 1],
            [1, 0, 0, 0, 325, 0, 1],
            [1, 0, 0, 0, 32, 321, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 92, 91, 1],
            [1, 0, 0, 0, 0, 812, 1],
            [1, 0, 0, 0, 0, 812, 1],
            [1, 0, 0, 0, 0, 93, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 7:
        //lvl7
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 32, 22, 21, 23, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 31, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 32, 322, 0, 0, 1],
            [1, 0, 321, 0, 0, 0, 1],
            [1, 0, 0, 0, 31, 311, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 94, 811, 811, 91, 1],
            [1, 0, 0, 0, 0, 93, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 8:
//lvl8
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 32, 0, 0, 0, 0, 1],
            [1, 0, 22, 0, 0, 0, 1],
            [1, 33, 21, 0, 0, 0, 1],
            [1, 0, 23, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 325, 0, 0, 0, 0, 1],
            [1, 32, 321, 0, 0, 0, 1],
            [1, 331, 0, 0, 0, 0, 1],
            [1, 33, 322, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 811, 91, 0, 0, 1],
            [1, 0, 0, 812, 0, 0, 1],
            [1, 0, 822, 93, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 9:
        //lvl9
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 31, 0, 0, 0, 0, 1],
            [1, 0, 23, 21, 22, 0, 1],
            [1, 0, 0, 33, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 32, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 31, 0, 0, 0, 0, 1],
            [1, 311, 0, 0, 0, 0, 1],
            [1, 0, 323, 33, 0, 0, 1],
            [1, 0, 325, 331, 0, 0, 1],
            [1, 0, 32, 321, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 812, 0, 0, 0, 0, 1],
            [1, 94, 821, 0, 0, 0, 1],
            [1, 0, 0, 812, 0, 0, 1],
            [1, 0, 0, 93, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 10:
        //lvl10
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 33, 0, 1],
            [1, 31, 0, 23, 21, 22, 1],
            [1, 0, 32, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 311, 0, 0, 33, 331, 1],
            [1, 31, 325, 0, 324, 0, 1],
            [1, 0, 32, 321, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 92, 91, 0, 0, 0, 1],
            [1, 0, 823, 0, 824, 0, 1],
            [1, 0, 0, 811, 93, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    //middel
    case 11:
//lvl17
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 33, 0, 0, 0, 1],
            [1, 0, 0, 32, 0, 0, 1],
            [1, 0, 0, 23, 0, 0, 1],
            [1, 0, 0, 21, 0, 0, 1],
            [1, 0, 0, 22, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 323, 33, 321, 0, 0, 1],
            [1, 0, [331, 323], 32, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 92, [812, 821], 0, 0, 0, 1],
            [1, 94, 93, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 12:
        //lvl18
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 32, 0, 1],
            [1, 22, 21, 23, 0, 31, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 321, 32, 311, 1],
            [1, 0, 0, 0, 324, 31, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 92, [821, 822], 91, 1],
            [1, 0, 0, 94, 0, 812, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 13:
        //lvl19
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 31, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 23, 21, 22, 0, 1],
            [1, 0, 0, 33, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 311, 31, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 325, 0, 0, 1],
            [1, 0, 331, 33, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 92, 811, 0, 0, 0, 1],
            [1, 94, 811, 91, 0, 0, 1],
            [1, 0, 0, 823, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 14:
        //lvl20
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 23, 0, 0, 1],
            [1, 0, 0, 21, 0, 0, 1],
            [1, 0, 0, 22, 0, 32, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 31, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 321, 32, 1],
            [1, 0, 0, 0, 0, 324, 1],
            [1, 0, 311, 31, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 92, 0, 1],
            [1, 0, 92, [821, 822], 93, 0, 1],
            [1, 0, 94, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 15:
        //lvl21
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 23, 21, 22, 0, 1],
            [1, 0, 31, 0, 33, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 32, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 311, 31, 323, 33, 0, 1],
            [1, 0, 311, 0, 331, 0, 1],
            [1, 0, 32, 322, 0, 0, 1],
            [1, 0, 321, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 92, 0, 0, 0, 0, 1],
            [1, 812, 0, 0, 812, 0, 1],
            [1, 812, 0, 822, 93, 0, 1],
            [1, 94, 93, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 16:
        //lvl22
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 31, 23, 21, 22, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 32, 0, 1],
            [1, 0, 0, 33, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 311, 31, 0, 0, 0, 1],
            [1, 0, 311, 0, 0, 0, 1],
            [1, 0, 0, 325, 32, 322, 1],
            [1, 0, 331, 33, 321, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 94, 91, 0, 0, 1],
            [1, 0, 0, 823, 0, 0, 1],
            [1, 0, 92, 0, 812, 0, 1],
            [1, 0, 94, 811, 93, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 17:
        //lvl23
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 31, 0, 0, 0, 1],
            [1, 0, 0, 0, 32, 0, 1],
            [1, 0, 33, 22, 21, 23, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 311, 31, 0, 0, 0, 1],
            [1, 0, [311, 325], 321, 32, 0, 1],
            [1, 331, 33, 0, 324, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 94, 811, 0, 0, 1],
            [1, 92, 0, 0, 824, 0, 1],
            [1, 94, 811, 811, 93, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 18:
        //lvl24
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 32, 22, 0, 1],
            [1, 0, 31, 0, 21, 0, 1],
            [1, 0, 0, 33, 23, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 321, 32, 0, 0, 1],
            [1, 311, 31, 324, 0, 0, 1],
            [1, 0, [311, 323], 33, 0, 0, 1],
            [1, 0, 0, 331, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 92, 811, 0, 0, 0, 1],
            [1, 94, 0, 0, 0, 0, 1],
            [1, 92, [812, 821], 0, 0, 0, 1],
            [1, 94, 93, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 19:
        //lvl25
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 33, 1],
            [1, 0, 0, 0, 0, 22, 1],
            [1, 0, 0, 32, 0, 21, 1],
            [1, 0, 0, 0, 0, 23, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 325, 1],
            [1, 0, 0, 0, 331, 33, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 32, 322, 0, 1],
            [1, 0, 0, 321, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 92, 0, 1],
            [1, 0, 0, 0, 812, 0, 1],
            [1, 0, 0, 0, 812, 0, 1],
            [1, 0, 0, 94, 93, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 20:
//lvl26
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 33, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 31, 0, 23, 21, 22, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 32, 1],
            [1, 0, 0, 0, 0, 324, 1],
            [1, 311, 0, 0, 0, 0, 1],
            [1, 31, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 92, 91, 0, 0, 824, 1],
            [1, 812, 94, 811, 811, 93, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    //hard
    case 21:
        //lvl33
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 22, 21, 23, 1],
            [1, 0, 0, 0, 33, 0, 1],
            [1, 0, 0, 32, 0, 0, 1],
            [1, 0, 31, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, [321, 323], 33, 0, 1],
            [1, 0, 323, 32, 331, 0, 1],
            [1, 311, 31, 0, 0, 0, 1],
            [1, 0, 311, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 92, 821, 0, 94, 91, 1],
            [1, 94, 0, 0, 0, [823, 824], 1],
            [1, 0, 94, 811, 811, 93, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 22:
        //lvl34
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 31, 0, 0, 0, 1],
            [1, 0, 0, 0, 32, 0, 1],
            [1, 33, 0, 0, 22, 0, 1],
            [1, 0, 0, 0, 21, 0, 1],
            [1, 0, 0, 0, 23, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 31, 311, 321, 0, 1],
            [1, 331, 311, 323, 32, 0, 1],
            [1, 33, 322, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 811, 91, 0, 1],
            [1, 0, 92, 821, 0, 0, 1],
            [1, 0, [812, 822], 91, 0, 0, 1],
            [1, 0, 812, 812, 0, 0, 1],
            [1, 0, 94, 93, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        break
    case 23:
        //lvl35
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 22, 0, 0, 1],
            [1, 0, 0, 21, 0, 0, 1],
            [1, 31, 0, 23, 0, 0, 1],
            [1, 0, 0, 0, 32, 0, 1],
            [1, 33, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 31, 311, 0, 321, 0, 1],
            [1, [311, 331], 0, 323, 32, 0, 1],
            [1, 33, 322, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 92, [821, 822], 91, 0, 1],
            [1, 0, 93, 0, 812, 0, 1],
            [1, 94, 811, 91, 0, 0, 1],
            [1, 0, 822, 93, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 24:
        //lvl36
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 33, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 31, 0, 1],
            [1, 0, 0, 23, 21, 22, 1],
            [1, 0, 0, 0, 32, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 331, 33, 0, 1],
            [1, 0, 0, 0, 311, 0, 1],
            [1, 0, 0, 311, 31, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 321, 32, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 92, 811, 0, 0, 1],
            [1, 0, 94, 811, 91, 0, 1],
            [1, 0, 92, 811, 0, 0, 1],
            [1, 0, [823, 824], 0, 0, 0, 1],
            [1, 0, 94, 811, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 25:
        //lvl37
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 22, 21, 23, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 33, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 31, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 323, 33, 0, 0, 1],
            [1, 0, 311, 331, 0, 0, 1],
            [1, 311, 31, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 92, 811, 811, 91, 0, 1],
            [1, [823, 824], 0, 0, 812, 0, 1],
            [1, 94, 91, 94, 93, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 26:
        //lvl38
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 32, 0, 1],
            [1, 0, 31, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 23, 22, 21, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 32, 322, 1],
            [1, 0, 31, 311, 321, 0, 1],
            [1, 0, 311, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 92, 811, 91, 0, 0, 1],
            [1, 812, 0, [823, 824], 0, 0, 1],
            [1, 812, 0, 94, 93, 0, 1],
            [1, 94, 93, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 27:
        //lvl39
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 33, 22, 21, 23, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 32, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 325, 0, 0, 0, 1],
            [1, 331, 33, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 32, 322, 1],
            [1, 0, 0, 0, 321, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 92, 811, 811, 91, 0, 1],
            [1, 94, 0, 0, [823, 824], 0, 1],
            [1, 0, 0, 0, 94, 91, 1],
            [1, 0, 0, 0, 0, 812, 1],
            [1, 0, 0, 0, 94, 93, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 28:
        //lvl40
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 23, 0, 0, 0, 31, 1],
            [1, 21, 0, 32, 0, 0, 1],
            [1, 22, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 325, 311, 31, 1],
            [1, 0, 0, 32, 321, 311, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 92, 91, 0, 0, 1],
            [1, 0, 812, 94, 811, 0, 1],
            [1, 0, 812, 0, 91, 0, 1],
            [1, 0, 94, [821, 822], 93, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 29:
        //lvl42
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 22, 0, 0, 0, 0, 1],
            [1, 21, 0, 0, 0, 0, 1],
            [1, 23, 0, 0, 0, 33, 1],
            [1, 0, 0, 31, 0, 0, 1],
            [1, 0, 0, 0, 0, 32, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 331, 1],
            [1, 0, 0, 0, 0, 33, 1],
            [1, 0, 311, 31, 0, 0, 1],
            [1, 0, 0, 311, 321, 32, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 92, 91, 1],
            [1, 0, 0, 0, 812, 812, 1],
            [1, 0, 92, [821, 822], 93, 0, 1],
            [1, 0, 94, 0, 0, 0, 1],
            [1, 0, 0, 94, 811, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
    case 30:
        //lvl43
        gridMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 33, 0, 0, 22, 1],
            [1, 0, 0, 0, 0, 21, 1],
            [1, 0, 0, 0, 0, 23, 1],
            [1, 0, 31, 0, 32, 0, 1],
            [1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        poortMatrix = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 33, 331, 0, 0, 1],
            [1, 0, 324, 0, 0, 0, 1],
            [1, 0, 0, 0, 325, 0, 1],
            [1, 311, 31, 0, 32, 321, 1],
            [1, 0, 311, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ];
        finalPieceAlign = [
            [1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 811, 91, 0, 1],
            [1, 0, 0, 92, 93, 0, 1],
            [1, 92, [821, 822], 93, 0, 0, 1],
            [1, 94, 0, 0, 0, 91, 1],
            [1, 0, 94, 811, 811, 93, 1],
            [1, 1, 1, 1, 1, 1, 1]
        ]
        break
}


const
    gridSystem = new GridSystem(gridMatrix, poortMatrix, finalPieceAlign);
gridSystem
    .render();
